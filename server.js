require('dotenv').config();
const express = require('express');
const cors = require('cors');

const multer = require('multer');

const app = express();

app.use(express.static('uploads'))
app.use(cors({
    origin: '*'
}));


//multer 

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '/uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        const splt = file.originalname.split('.');
        const suffix = splt[splt.length - 1]; //fil.easd.na.jpg

        cb(null, file.fieldname + '-' + uniqueSuffix + '.' + suffix)
    }
})

const upload = multer({
    storage: storage
})

app.post('/files', upload.single('image'), (req, res, next) => {
    console.log(req.file);

    res.send('uploaded')
})
//...

const { PORT } = process.env;

app.listen(PORT, () => {
    console.log(`files server is running on ${PORT}`);
})